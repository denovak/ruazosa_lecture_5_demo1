package hr.fer.ruazosa.lecture5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startLongRunningOperationButton.setOnClickListener {

            Thread {
                for (i in 1..10) {
                    Thread.sleep(1000)
                    runOnUiThread {
                        operationStatusTextView.text = i.toString()
                    }

                }
            }.start()

        }

        sayHelloButton.setOnClickListener {
            val toastMessage = Toast.makeText(applicationContext, "Hello world", Toast.LENGTH_LONG)
            toastMessage.show()

        }


    }
}
